## Customer Statement Processor Back-End

Microservice that processes XML and CSV files that contain customer statement records. The service has a REST endpoint to 
upload files and an endpoint to retrieve the validation reports of uploaded files. 
The processing of a statement file consists of:
- check that the mime type of the file is indeed XML or CSV
- convert the file content into a collection of Java objects
- validate this collection and create a validation report counting the number of duplicate transaction references and 
invalid balances and a list of the failed records
- store the report into H2 database

In the project directory, you can run (default port 8080):

### `mvn spring-boot:run`

Swagger documentation can be found:

### `localhost:8080/api/swagger-ui.html`

H2 database console:

### `localhost:8080/api/h2-console`

Test files and scripts can be found in folder:

### `src/main/resources/`

A unit test (only one) can be run:

### `mvn test`



 
