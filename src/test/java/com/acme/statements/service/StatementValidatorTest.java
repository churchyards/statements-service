package com.acme.statements.service;

import com.acme.statements.model.FailedStatement;
import com.acme.statements.model.Statement;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class StatementValidatorTest {

    private static final List<Statement> statementListWithDuplicates = new ArrayList<>();

    private StatementValidator underTest = new StatementValidator();

    @Test
    public void whenValidateDuplicatesThenInvalidsAreReturned() {
        List<FailedStatement> resultList = underTest.validate(statementListWithDuplicates());
        assertEquals(2, resultList.size());
        assertTrue(resultList.get(0).isDuplicate());
        assertTrue(resultList.get(1).isDuplicate());
    }

    @Test
    public void whenValidateInvalidBalancesThenInvalidsAreReturned() {
        List<FailedStatement> resultList = underTest.validate(statementListWithInvalidBalance());
        assertEquals(1, resultList.size());
        assertTrue(resultList.get(0).isInvalidBalance());
    }

    private List<Statement> statementListWithDuplicates() {
        final List<Statement> list = new ArrayList<>();
        list.add(Statement.builder()
                .transactionReference(1L)
                .accountNumber("FOO")
                .startBalance(new BigDecimal(10))
                .mutation(new BigDecimal(10))
                .endBalance(new BigDecimal(20))
                .description("FOO")
                .build()
        );
        list.add(Statement.builder()
                .transactionReference(2L)
                .accountNumber("BAR")
                .startBalance(new BigDecimal(10))
                .mutation(new BigDecimal(10))
                .endBalance(new BigDecimal(20))
                .description("BAR")
                .build()
        );
        list.add(Statement.builder()
                .transactionReference(1L)
                .accountNumber("FOO")
                .startBalance(new BigDecimal(10))
                .mutation(new BigDecimal(10))
                .endBalance(new BigDecimal(20))
                .description("FOO")
                .build()
        );
        return list;
    }

    private List<Statement> statementListWithInvalidBalance() {
        final List<Statement> list = new ArrayList<>();
        list.add(Statement.builder()
                .transactionReference(1L)
                .accountNumber("FOO")
                .startBalance(new BigDecimal(10))
                .mutation(new BigDecimal(10))
                .endBalance(new BigDecimal(30))
                .description("FOO")
                .build()
        );
        list.add(Statement.builder()
                .transactionReference(2L)
                .accountNumber("BAR")
                .startBalance(new BigDecimal(10))
                .mutation(new BigDecimal(10))
                .endBalance(new BigDecimal(20))
                .description("BAR")
                .build()
        );
        list.add(Statement.builder()
                .transactionReference(3L)
                .accountNumber("FOO")
                .startBalance(new BigDecimal(10))
                .mutation(new BigDecimal(10))
                .endBalance(new BigDecimal(20))
                .description("FOO")
                .build()
        );
        return list;
    }
}
