package com.acme.statements.service;

import com.google.common.io.Files;
import org.apache.tika.Tika;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

/**
 * This component tries to detect the content type of a file by using Tika. In case of text/plain and file extention 'csv',
 * it is assumed that the file is indeed a CSV file.
 */
@Component
public class ContentTypeDetector {
    private final static String PLAIN_TEXT = "text/plain";

    private final Tika tika;

    public ContentTypeDetector() {
        tika = new Tika();
    }

    AllowedContentType getType(final MultipartFile file) {
        try {
            String mimeType = tika.detect(file.getBytes());
            if (mimeType.equalsIgnoreCase(PLAIN_TEXT) && file.getOriginalFilename() != null) {
                mimeType = Files.getFileExtension(file.getOriginalFilename());
            }
            return AllowedContentType.fromName(mimeType);
        } catch (final Throwable e) {
            throw new InvalidFileException(e.getMessage(), e);
        }
    }
}
