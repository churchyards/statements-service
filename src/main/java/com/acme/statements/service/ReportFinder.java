package com.acme.statements.service;

import com.acme.statements.infrastructure.jpa.ReportEntity;
import com.acme.statements.infrastructure.jpa.ReportRepository;
import com.acme.statements.model.Report;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ReportFinder {

    private ReportRepository reportRepository;

    public List<Report> findAll() {
        return reportRepository.findAll().stream()
                .map(ReportEntity::toDomain)
                .sorted( Comparator.comparing(Report::getTimestamp) )
                .collect(Collectors.toList());
    }
}
