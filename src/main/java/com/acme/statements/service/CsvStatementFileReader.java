package com.acme.statements.service;

import com.acme.statements.model.Statement;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Utility component that reads a CSV file and maps its rows to a list of {@link com.acme.statements.model.Statement} beans.
 * Assumptions:
 * - First row is a header
 * - The columns are comma separated
 * - The order of the attributes in a row is fixed
 */
@Component
public class CsvStatementFileReader implements StatementFileReader {
    private static final String COMMA = ",";

    @Override
    public List<Statement> read(final MultipartFile file) {
        try {
            final BufferedReader br = new BufferedReader(new InputStreamReader(file.getInputStream()));
            return br.lines().skip(1).map(toStatement).collect(Collectors.toList());
        } catch(final Throwable e) {
            throw new InvalidFileException(e.getMessage(), e);
        }
    }

    private final Function<String, Statement> toStatement = (line) -> {
        final String[] cols = line.split(COMMA);
        return Statement.builder()
                .transactionReference(Long.valueOf(cols[0]))
                .accountNumber(cols[1])
                .description(cols[2])
                .startBalance(new BigDecimal(cols[3]))
                .mutation(new BigDecimal(cols[4]))
                .endBalance(new BigDecimal(cols[5]))
                .build();
    };
}
