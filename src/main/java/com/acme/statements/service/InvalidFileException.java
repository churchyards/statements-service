package com.acme.statements.service;

public class InvalidFileException extends RuntimeException {

    public InvalidFileException(final String message) {
        super(message);
    }

    public InvalidFileException(final String message, final Throwable e) {
        super(message, e);
    }
}
