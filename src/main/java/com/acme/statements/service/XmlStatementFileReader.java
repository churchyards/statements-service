package com.acme.statements.service;

import com.acme.statements.model.Statement;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class XmlStatementFileReader implements StatementFileReader {

    @Override
    public List<Statement> read(final MultipartFile file) {
        try {
            final JAXBContext context = JAXBContext.newInstance(Records.class);
            final Unmarshaller unmarshaller = context.createUnmarshaller();
            final Records records = (Records)unmarshaller.unmarshal(file.getInputStream());
            return records.getRecord().stream()
                    .map(toStatement)
                    .collect(Collectors.toList());
        } catch(final Throwable e) {
            throw new InvalidFileException(e.getMessage(), e);
        }
    }

    private final Function<Record, Statement> toStatement = (record) -> Statement.builder()
            .transactionReference(record.getReference())
            .accountNumber(record.getAccountNumber())
            .description(record.getDescription())
            .startBalance(record.getStartBalance())
            .mutation(record.getMutation())
            .endBalance(record.getEndBalance())
            .build();

    @XmlRootElement
    private static class Records {
        private List<Record> record;

        List<Record> getRecord() {
            return record;
        }

        @XmlElement
        public void setRecord(List<Record> record) {
            this.record = record;
        }
    }

    private static class Record {
        private Long reference;
        private String accountNumber;
        private String description;
        private BigDecimal startBalance;
        private BigDecimal mutation;
        private BigDecimal endBalance;

        Long getReference() {
            return reference;
        }

        @XmlAttribute
        public void setReference(Long reference) {
            this.reference = reference;
        }

        String getAccountNumber() {
            return accountNumber;
        }

        @XmlElement
        public void setAccountNumber(String accountNumber) {
            this.accountNumber = accountNumber;
        }

        String getDescription() {
            return description;
        }

        @XmlElement
        public void setDescription(String description) {
            this.description = description;
        }

        BigDecimal getStartBalance() {
            return startBalance;
        }

        @XmlElement
        public void setStartBalance(BigDecimal startBalance) {
            this.startBalance = startBalance;
        }

        BigDecimal getMutation() {
            return mutation;
        }

        @XmlElement
        public void setMutation(BigDecimal mutation) {
            this.mutation = mutation;
        }

        BigDecimal getEndBalance() {
            return endBalance;
        }

        @XmlElement
        public void setEndBalance(BigDecimal endBalance) {
            this.endBalance = endBalance;
        }
    }
}
