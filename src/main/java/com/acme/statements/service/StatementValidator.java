package com.acme.statements.service;

import com.acme.statements.model.FailedStatement;
import com.acme.statements.model.Statement;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class StatementValidator {

    public List<FailedStatement> validate(final List<Statement> statements) {
        final Map<Long, Long> map = statements.stream()
                .collect(Collectors.groupingBy(Statement::getTransactionReference, Collectors.counting()));

        final List<Long> duplicates = map.entrySet().stream()
                .filter( e -> e.getValue() > 1 )
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());

        return statements.stream()
                .filter( s -> isDuplicate(s, duplicates) || isInvalidBalance(s) )
                .map( s -> FailedStatement.childBuilder()
                        .isDuplicate(isDuplicate(s, duplicates))
                        .isInvalidBalance(isInvalidBalance(s))
                        .statement(s)
                        .build()
                )
                .collect(Collectors.toList());
    }

    private boolean isDuplicate(final Statement statement, final List<Long> duplicates) {
        return duplicates.contains(statement.getTransactionReference());
    }

    private boolean isInvalidBalance(final Statement statement) {
        final BigDecimal computedBalance = statement.getStartBalance().add(statement.getMutation());
        return !statement.getEndBalance().equals(computedBalance);
    }
}
