package com.acme.statements.service;

import com.acme.statements.model.Statement;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface StatementFileReader {

   List<Statement> read(final MultipartFile file);
}
