package com.acme.statements.service;

import com.acme.statements.infrastructure.jpa.ReportEntity;
import com.acme.statements.infrastructure.jpa.ReportRepository;
import com.acme.statements.model.FailedStatement;
import com.acme.statements.model.Report;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Service
@AllArgsConstructor
public class Reporter {

    private ReportRepository reportRepository;

    @Transactional
    public Report createReport(final String fileName, final Integer numberOfRecords, final List<FailedStatement> failed) {
        ReportEntity entity = ReportEntity.builder()
                .fileName(fileName)
                .timestamp(LocalDateTime.now())
                .numberOfRecords(numberOfRecords)
                .numberOfFailedRecords(failed.size())
                .numberOfDuplicates((int) failed.stream().filter(FailedStatement::isDuplicate).count())
                .numberOfInvalidBalances((int) failed.stream().filter(FailedStatement::isInvalidBalance).count())
                .failedStatements(failed)
                .build();
        entity = reportRepository.save(entity);
        return entity.toDomain();
    }
}
