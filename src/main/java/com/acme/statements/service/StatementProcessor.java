package com.acme.statements.service;

import com.acme.statements.model.FailedStatement;
import com.acme.statements.model.Report;
import com.acme.statements.model.Statement;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
@AllArgsConstructor
public class StatementProcessor {

    private ContentTypeDetector contentTypeDetector;
    private StatementFileReader csvStatementFileReader;
    private StatementFileReader xmlStatementFileReader;
    private StatementValidator statementValidator;
    private Reporter reporter;

    public Report process(final MultipartFile file) {
        final AllowedContentType contentType = contentTypeDetector.getType(file);

        if (contentType == null) {
            throw new InvalidFileException("Only CSV and XML files are supported");
        }

        final List<Statement> statements = getStatements(file, contentType);

        final List<FailedStatement> failed = statementValidator.validate(statements);

        return reporter.createReport(file.getOriginalFilename(), statements.size(), failed);
    }

    private List<Statement> getStatements(final MultipartFile file, final AllowedContentType contentType) {
        switch(contentType) {
            case APPLICATION_XML:
            case TEXT_XML:
                return xmlStatementFileReader.read(file);
            case APPLICATION_CSV:
            case TEXT_CSV:
            case PLAIN_TEXT_CSV:
                return csvStatementFileReader.read(file);
            default:
                throw new InvalidFileException("Only CSV and XML files are supported");
        }
    }
}
