package com.acme.statements.service;

import lombok.Getter;

@Getter
public enum AllowedContentType {
    TEXT_XML("text/xml"),
    APPLICATION_XML("application/xml"),
    TEXT_CSV("text/csv"),
    APPLICATION_CSV("application/csv"),
    PLAIN_TEXT_CSV("csv");

    private final String name;

    AllowedContentType(final String name) {
        this.name = name;
    }

    public static AllowedContentType fromName(String name) {
        for (AllowedContentType value : AllowedContentType.values()) {
            if (value.name.equalsIgnoreCase(name)) {
                return value;
            }
        }
        return null;
    }
}
