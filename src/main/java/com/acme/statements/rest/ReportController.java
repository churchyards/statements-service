package com.acme.statements.rest;

import com.acme.statements.model.Report;
import com.acme.statements.service.ReportFinder;
import com.acme.statements.service.StatementProcessor;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Api(
        value = "Service that can be used to create and request statement validation reports",
        description = "Service that can be used to create and request statement validation reports"
)
@Slf4j
@RestController
@RequestMapping(value = "/v1/reports")
@AllArgsConstructor
public class ReportController {

    private static final Logger LOG =   LoggerFactory.getLogger(ReportController.class);

    private StatementProcessor statementProcessor;
    private ReportFinder reportFinder;

    @ApiOperation(
            value = "Create report for uploaded file",
            notes = "Processes an uploaded statement file. Statements are validated and a report of the validation result is stored in the database. Supported file types are CSV and XML.",
            response = Report.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully created report"),
            @ApiResponse(code = 400, message = "The uploaded file was missing, empty or invalid"),
            @ApiResponse(code = 500, message = "Unexpected error has occurred")
    })
    @PostMapping
    public ResponseEntity<Report> uploadFile(@RequestParam("file") MultipartFile file) {
        LOG.info(String.format("uploadFile started for file '%s'", file.getOriginalFilename()));

        if (file.isEmpty()) {
            throw new MissingFileException();
        }

        final Report report = statementProcessor.process(file);

        return new ResponseEntity<>(report, new HttpHeaders(), HttpStatus.CREATED);
    }

    @ApiOperation(
            value = "Get all reports",
            response = Report.class,
            responseContainer = "List"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Sent list of all reports or an empty list when not found"),
            @ApiResponse(code = 500, message = "Unexpected error has occurred")
    })
    @GetMapping
    public ResponseEntity<List<Report>> getAllReports() {
        final List<Report> reports = reportFinder.findAll();
        return new ResponseEntity<>(reports, new HttpHeaders(), HttpStatus.OK);
    }
}
