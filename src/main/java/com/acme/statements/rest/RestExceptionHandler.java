package com.acme.statements.rest;

import com.acme.statements.service.InvalidFileException;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger LOG =   LoggerFactory.getLogger(RestExceptionHandler.class);

    @ExceptionHandler(Throwable.class)
    private ResponseEntity<String> handleException(final Throwable e) {
        LOG.error("Error occurred", e);

        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        if (e instanceof MultipartException) {
            status = HttpStatus.BAD_REQUEST;
        } else if (e instanceof MissingFileException) {
            status = HttpStatus.BAD_REQUEST;
        } else if (e instanceof InvalidFileException) {
            status = HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity<>(e.getMessage(), status);
    }
}


