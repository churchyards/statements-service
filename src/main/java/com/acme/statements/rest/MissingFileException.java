package com.acme.statements.rest;

class MissingFileException extends RuntimeException {

    MissingFileException() {
        super("No file has been chosen or the chosen file has no content");
    }

}
