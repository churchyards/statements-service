package com.acme.statements.infrastructure.jpa;

import com.acme.statements.model.FailedStatement;
import com.acme.statements.model.Report;
import com.acme.statements.model.Statement;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "REPORTS")
@Data
@NoArgsConstructor
public class ReportEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column(name = "FILE_NAME")
    @NotNull
    private String fileName;

    @NotNull
    private LocalDateTime timestamp;

    @Column(name = "NUM_RECORDS")
    @NotNull
    private Integer numberOfRecords;

    @Column(name = "NUM_FAILED_RECORDS")
    @NotNull
    private Integer numberOfFailedRecords;

    @Column(name = "NUM_DUPLICATES")
    @NotNull
    private Integer numberOfDuplicates;

    @Column(name = "NUM_INVALID_BALANCES")
    @NotNull
    private Integer numberOfInvalidBalances;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "REPORT_ID", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private List<FailedStatementEntity> failedStatementEntities;

    @Builder
    private ReportEntity(final String fileName,
                         final LocalDateTime timestamp,
                         final Integer numberOfRecords,
                         final Integer numberOfFailedRecords,
                         final Integer numberOfDuplicates,
                         final Integer numberOfInvalidBalances,
                         final List<FailedStatement> failedStatements
                         ) {
        this.fileName = fileName;
        this.timestamp = timestamp;
        this.numberOfRecords = numberOfRecords;
        this.numberOfFailedRecords = numberOfFailedRecords;
        this.numberOfDuplicates = numberOfDuplicates;
        this.numberOfInvalidBalances = numberOfInvalidBalances;
        failedStatementEntities = failedStatements.stream()
                .map( fs -> FailedStatementEntity.builder()
                        .isDuplicate(fs.isDuplicate())
                        .isInvalidBalance(fs.isInvalidBalance())
                        .statement(Statement.builder()
                                .transactionReference(fs.getTransactionReference())
                                .accountNumber(fs.getAccountNumber())
                                .startBalance(fs.getStartBalance())
                                .mutation(fs.getMutation())
                                .endBalance(fs.getEndBalance())
                                .description(fs.getDescription())
                                .build()
                        )
                        .build()
                )
                .collect(Collectors.toList());
    }

    public Report toDomain() {
        return Report.builder()
                .fileName(fileName)
                .timestamp(timestamp)
                .numberOfRecords(numberOfRecords)
                .numberOfFailedRecords(numberOfFailedRecords)
                .numberOfDuplicates(numberOfDuplicates)
                .numberOfInvalidBalances(numberOfInvalidBalances)
                .failedRecords(failedStatementEntities.stream()
                        .map(fse -> FailedStatement.childBuilder()
                                .isDuplicate(fse.getIsDuplicate())
                                .isInvalidBalance(fse.getIsInvalidBalance())
                                .statement(Statement.builder()
                                        .transactionReference(fse.getTransactionReference())
                                        .accountNumber(fse.getAccountNumber())
                                        .startBalance(fse.getStartBalance())
                                        .mutation(fse.getMutation())
                                        .endBalance(fse.getEndBalance())
                                        .description(fse.getDescription())
                                        .build())
                                .build())
                        .collect(Collectors.toList())
                )
                .build();
    }
}
