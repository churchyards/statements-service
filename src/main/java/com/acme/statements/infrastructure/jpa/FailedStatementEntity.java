package com.acme.statements.infrastructure.jpa;

import com.acme.statements.model.Statement;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
@Table(name = "FAILED_STATEMENTS")
@Data
@NoArgsConstructor
class FailedStatementEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column(name = "DUPLICATE")
    @NotNull
    private Boolean isDuplicate;

    @Column(name = "INVALID_BALANCE")
    @NotNull
    private Boolean isInvalidBalance;

    @Column(name = "TRANSACTION_REFERENCE")
    @NotNull
    private Long transactionReference;

    @Column(name = "ACCOUNT_NUMBER")
    @NotNull
    private String accountNumber;

    @Column(name = "START_BALANCE")
    @NotNull
    private BigDecimal startBalance;

    @NotNull
    private BigDecimal mutation;

    @Column(name = "END_BALANCE")
    @NotNull
    private BigDecimal endBalance;

    @NotNull
    private String description;

    @Builder
    private FailedStatementEntity(final Boolean isDuplicate,
                                  final Boolean isInvalidBalance,
                                  final Statement statement
                         ) {
        this.isDuplicate = isDuplicate;
        this.isInvalidBalance = isInvalidBalance;
        transactionReference = statement.getTransactionReference();
        accountNumber = statement.getAccountNumber();
        startBalance = statement.getStartBalance();
        mutation = statement.getMutation();
        endBalance = statement.getEndBalance();
        description = statement.getDescription();
    }
}
