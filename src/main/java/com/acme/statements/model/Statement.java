package com.acme.statements.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
public class Statement {

    @NotBlank
    private Long transactionReference;

    @NotBlank
    private String accountNumber;

    @NotBlank
    private BigDecimal startBalance;

    @NotBlank
    private BigDecimal mutation;

    @NotBlank
    private BigDecimal endBalance;

    @NotBlank
    private String description;

}
