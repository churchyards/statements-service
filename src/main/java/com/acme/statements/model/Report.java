package com.acme.statements.model;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
public class Report {

    @NotBlank
    private String fileName;

    @NotBlank
    private LocalDateTime timestamp;

    @NotBlank
    private Integer numberOfRecords;

    @NotBlank
    private Integer numberOfFailedRecords;

    @NotBlank
    private Integer numberOfDuplicates;

    @NotBlank
    private Integer numberOfInvalidBalances;

    private List<FailedStatement> failedRecords;

}
