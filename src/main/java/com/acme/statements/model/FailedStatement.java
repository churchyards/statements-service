package com.acme.statements.model;

import lombok.Builder;
import lombok.Data;

@Data
public class FailedStatement extends Statement {

    private boolean isDuplicate;

    private boolean isInvalidBalance;

    @Builder(builderMethodName = "childBuilder")
    private FailedStatement(final boolean isDuplicate, final boolean isInvalidBalance, final Statement statement) {
        super(statement.getTransactionReference(),
                statement.getAccountNumber(),
                statement.getStartBalance(),
                statement.getMutation(),
                statement.getEndBalance(),
                statement.getDescription()
        );
        this.isDuplicate = isDuplicate;
        this.isInvalidBalance = isInvalidBalance;
    }
}
